import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { CategoryService, GetResponseCategory } from 'src/app/_services/category/category.service';
import { Category } from 'src/app/_models/category';
import { ActivatedRoute } from '@angular/router';
import { Page } from 'src/app/_models/page';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { merge, of as observableOf, Subject } from 'rxjs';
import { startWith, tap, catchError } from 'rxjs/operators';
import { CategorySearch } from 'src/app/_models/categorySearch';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CategoryComponent } from 'src/app/_component/category/category.component';
import { NotificationService } from 'src/app/_services/notification/notification.service';
import { ConfirmDialogComponent } from 'src/app/_component/confirm-dialog/confirm-dialog.component';


@Component({
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['select', 'name'];
  dialogConfig: MatDialogConfig = new MatDialogConfig();
  confirmDialogConfig: MatDialogConfig = new MatDialogConfig();

  private update = new Subject<void>();
  update$ = this.update.asObservable();

  level0Data: MatTableDataSource<Category>;
  level0IsLoadingResults = false;
  level0DataSelection = new SelectionModel<Category>(true, []);
  level0SearchSubject = new Subject<string>();
  level0SearchObs$ = this.level0SearchSubject.asObservable();
  level0Page: Page = new Page(5, 0);
  level0SearchKeyword: string = null;
  @ViewChild('level0Paginator') level0Paginator: MatPaginator;
  @ViewChild('level0Sort') level0Sort: MatSort;

  level1Data: MatTableDataSource<Category>;
  level1IsLoadingResults = false;
  level1DataSelection = new SelectionModel<Category>(true, []);
  level1SearchSubject = new Subject<string>();
  level1SearchObs$ = this.level1SearchSubject.asObservable();
  level1ParentIds: string[];
  level1ParentSubject = new Subject<number[]>();
  level1ParentObs$ = this.level1ParentSubject.asObservable();
  level1Page: Page = new Page(5, 0);
  level1SearchKeyword: string = null;
  @ViewChild('level1Paginator') level1Paginator: MatPaginator;
  @ViewChild('level1Sort') level1Sort: MatSort;

  level2Data: MatTableDataSource<Category>;
  level2IsLoadingResults = false;
  level2DataSelection = new SelectionModel<Category>(true, []);
  level2SearchSubject = new Subject<string>();
  level2SearchObs$ = this.level2SearchSubject.asObservable();
  level2ParentIds: string[];
  level2ParentSubject = new Subject<number[]>();
  level2ParentObs$ = this.level2ParentSubject.asObservable();
  level2Page: Page = new Page(5, 0);
  level2SearchKeyword: string = null;
  @ViewChild('level2Paginator') level2Paginator: MatPaginator;
  @ViewChild('level2Sort') level2Sort: MatSort;


  constructor(
    private categoryService: CategoryService,
    private notificationService: NotificationService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;

    this.confirmDialogConfig.disableClose = true;
    this.confirmDialogConfig.autoFocus = true;
    this.confirmDialogConfig.width = '400px';

  }

  ngAfterViewInit() {
    this.update$.subscribe(() => { this.refresh(); });
    this.update.next();
  }

  refresh() {
    this.level0Sort.sortChange.subscribe(() => this.level0Paginator.pageIndex = 0);
    this.level0SearchObs$.subscribe(() => this.level0Paginator.pageIndex = 0);

    merge(this.level0Sort.sortChange, this.level0Paginator.page, this.level0SearchObs$)
      .pipe(
        startWith({}),
        tap(() => {
          this.level0IsLoadingResults = true;
        })
      )
      .subscribe(() => {
        let sort: string = null;
        const categorySearch: CategorySearch = new CategorySearch();
        categorySearch.name = this.level0SearchKeyword;
        categorySearch.categoryLevel = 0;
        if (this.level0Sort.active && this.level0Sort.direction) {
          sort = this.level0Sort.active + ',' + this.level0Sort.direction;
        }
        this.categoryService.getCategoryList(
          categorySearch,
          null,
          this.level0Paginator.pageIndex,
          this.level0Paginator.pageSize,
          sort).pipe(
            catchError(() => {
              this.level0IsLoadingResults = false;
              return observableOf([]);
            }))
          .subscribe((data: GetResponseCategory) => {
            this.level0IsLoadingResults = false;
            this.level0Data = new MatTableDataSource(data._embedded.categories);
            const selecteds: Category[] = this.level0DataSelection.selected;
            if (data._embedded.categories
              && data._embedded.categories.length
              && selecteds
              && selecteds.length) {
              data._embedded.categories.forEach(cat => {
                selecteds.forEach(selected => {
                  if (cat.id === selected.id) {
                    this.level0DataSelection.deselect(selected);
                    this.level0DataSelection.toggle(cat);
                  }
                });
              });
            }
            this.level0Page = data.page;

          });
      });

    this.level1Sort.sortChange.subscribe(() => this.level1Paginator.pageIndex = 0);
    this.level1SearchObs$.subscribe(() => this.level1Paginator.pageIndex = 0);

    merge(this.level1Sort.sortChange, this.level1Paginator.page, this.level1SearchObs$, this.level1ParentObs$)
      .pipe(
        startWith({}),
        tap(() => {
          this.level1IsLoadingResults = true;
        })
      )
      .subscribe(() => {
        let sort: string = null;
        const categorySearch: CategorySearch = new CategorySearch();
        categorySearch.name = this.level1SearchKeyword;
        categorySearch.categoryLevel = 1;
        if (this.level1Sort.active && this.level1Sort.direction) {
          sort = this.level1Sort.active + ',' + this.level1Sort.direction;
        }
        this.categoryService.getCategoryList(
          categorySearch,
          this.level1ParentIds,
          this.level1Paginator.pageIndex,
          this.level1Paginator.pageSize,
          sort).pipe(
            catchError(() => {
              this.level1IsLoadingResults = false;
              return observableOf([]);
            }))
          .subscribe((data: GetResponseCategory) => {
            this.level1IsLoadingResults = false;
            this.level1Data = new MatTableDataSource(data._embedded.categories);
            const selecteds: Category[] = this.level1DataSelection.selected;
            if (data._embedded.categories
              && data._embedded.categories.length
              && selecteds
              && selecteds.length) {
              data._embedded.categories.forEach(cat => {
                selecteds.forEach(selected => {
                  if (cat.id === selected.id) {
                    this.level1DataSelection.deselect(selected);
                    this.level1DataSelection.toggle(cat);
                  }
                });
              });
            }
            this.level1Page = data.page;

          });
      });

    this.level2Sort.sortChange.subscribe(() => this.level2Paginator.pageIndex = 0);
    this.level2SearchObs$.subscribe(() => this.level2Paginator.pageIndex = 0);

    merge(this.level2Sort.sortChange, this.level2Paginator.page, this.level2SearchObs$, this.level2ParentObs$)
      .pipe(
        startWith({}),
        tap(() => {
          this.level2IsLoadingResults = true;
        })
      )
      .subscribe(() => {
        let sort: string = null;
        const categorySearch: CategorySearch = new CategorySearch();
        categorySearch.name = this.level2SearchKeyword;
        categorySearch.categoryLevel = 2;
        if (this.level2Sort.active && this.level2Sort.direction) {
          sort = this.level2Sort.active + ',' + this.level2Sort.direction;
        }
        this.categoryService.getCategoryList(
          categorySearch,
          this.level2ParentIds,
          this.level2Paginator.pageIndex,
          this.level2Paginator.pageSize,
          sort).pipe(
            catchError(() => {
              this.level2IsLoadingResults = false;
              return observableOf([]);
            }))
          .subscribe((data: GetResponseCategory) => {
            this.level2IsLoadingResults = false;
            this.level2Data = new MatTableDataSource(data._embedded.categories);
            const selecteds: Category[] = this.level2DataSelection.selected;
            if (data._embedded.categories
              && data._embedded.categories.length
              && selecteds
              && selecteds.length) {
              data._embedded.categories.forEach(cat => {
                selecteds.forEach(selected => {
                  if (cat.id === selected.id) {
                    this.level2DataSelection.deselect(selected);
                    this.level2DataSelection.toggle(cat);
                  }
                });
              });
            }
            this.level2Page = data.page;

          });
      });
  }

  level0DataSelectionToggle(row: Category) {
    this.level1SearchKeyword = '';
    this.level1ParentIds = null;
    this.level2SearchKeyword = '';
    this.level2ParentIds = null;

    this.level0DataSelection.toggle(row);
    this.level1DataSelection.clear();
    this.level1ParentIds = [];
    this.level2DataSelection.clear();
    this.level2ParentIds = [];
    const selecteds: Category[] = this.level0DataSelection.selected;
    if (selecteds && selecteds.length) {
      selecteds.forEach(selected => {
        this.level1ParentIds.push(selected.id);
      });
    } else {
      this.level2SearchSubject.next(this.level2SearchKeyword);
    }
    this.level1SearchSubject.next(this.level1SearchKeyword);
  }


  level1DataSelectionToggle(row: Category) {
    this.level2SearchKeyword = '';
    this.level2ParentIds = null;

    this.level1DataSelection.toggle(row);
    const selecteds: Category[] = this.level1DataSelection.selected;

    if (selecteds && selecteds.length) {
      this.level2ParentIds = [];
      selecteds.forEach(selected => {
        this.level2ParentIds.push(selected.id);
      });
    }
    this.level2SearchSubject.next(this.level2SearchKeyword);
  }

  level2SerachClear() {
    this.level2SearchKeyword = '';
    this.level2SearchSubject.next(this.level2SearchKeyword);
  }
  level2ApplayFilter(event) {
    if (event.key === 'Enter') {
      this.level2SearchSubject.next(this.level2SearchKeyword);
    }
  }
  level2DataSelectionToggle(row: Category) {
    this.level2DataSelection.toggle(row);
  }

  level1SerachClear() {
    this.level1SearchKeyword = '';
    this.level1SearchSubject.next(this.level1SearchKeyword);
  }
  level1ApplayFilter(event) {
    if (event.key === 'Enter') {
      this.level1SearchSubject.next(this.level1SearchKeyword);
    }
  }
  level0SerachClear() {
    this.level0SearchKeyword = '';
    this.level0SearchSubject.next(this.level0SearchKeyword);
  }
  level0ApplayFilter(event) {
    if (event.key === 'Enter') {
      this.level0SearchSubject.next(this.level0SearchKeyword);
    }
  }

  categoryAdd(level: number) {
    this.categoryService.initForm(level);
    this.dialogConfig.data = {};
    this.dialog.open(CategoryComponent, this.dialogConfig).afterClosed().subscribe(result => {
      if (result) {
        this.notificationService.success('Insert Category success');
        this.update.next();
      }
    });
  }

  categoryEdit(row: Category) {
    this.dialogConfig.data = {};
    this.categoryService.populateForm(row);
    this.dialog.open(CategoryComponent, this.dialogConfig).afterClosed().subscribe(result => {
      if (result) {
        this.notificationService.success('Update Category success');
        this.update.next();
      }
    });
  }

  categoryRemove(row: Category) {
    this.confirmDialogConfig.panelClass = 'confirm-dialog-container';
    this.confirmDialogConfig.position = { top: '10px' };
    this.confirmDialogConfig.data = { message: 'Are you sure to delete this category ?' };
    this.dialog.open(ConfirmDialogComponent, this.confirmDialogConfig).afterClosed().subscribe(result => {
      if (result) {
        this.categoryService.removeCategory(row.id).subscribe(() => {
          this.notificationService.success('Remove Category success');
          this.update.next();
        });
      }
    });
  }
}
