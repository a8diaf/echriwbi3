import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models/user';
import { AuthenticationService } from 'src/app/_services/authentication/authentication.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  currentuser: User;
  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.currentuser = this.authenticationService.currentUserValue;
  }

}
