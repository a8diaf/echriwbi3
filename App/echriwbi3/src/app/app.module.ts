import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthenticationService } from './_services/authentication/authentication.service';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { LoginComponent } from './_pages/login/login.component';
import { HomeComponent } from './_pages/home/home.component';
import { AdminComponent } from './_pages/admin/admin.component';
import { MaterialModule } from './_core/material.module';
import { CategoryComponent } from './_component/category/category.component';
import { ArticleComponent } from './_component/article/article.component';
import { AccountComponent } from './_pages/account/account.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { CategoriesComponent } from './_pages/categories/categories.component';
import { ArticlesComponent } from './_pages/articles/articles.component';
import { TagsComponent } from './_pages/tags/tags.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { ConfirmDialogComponent } from './_component/confirm-dialog/confirm-dialog.component';
import { BlogListComponent } from './_component/blog-list/blog-list.component';
import { BlogItemComponent } from './_component/blog-item/blog-item.component';
import { PostSamurayComponent } from './_component/post-samuray/post-samuray.component';

import { SlugifyPipe } from 'angular-pipes';
import { BlogCategoryComponent } from './_component/blog-category/blog-category.component';
import { BlogTagComponent } from './_component/blog-tag/blog-tag.component';
import { BlogNavComponent } from './_component/blog-nav/blog-nav.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BlogContactComponent } from './_component/blog-contact/blog-contact.component';
import { BlogAdminComponent } from './_component/blog-admin/blog-admin.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { PostsAdminComponent } from './_component/posts-admin/posts-admin.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    HomeComponent,
    AdminComponent,
    AdminMenuComponent,
    CategoryComponent,
    ArticleComponent,
    AccountComponent,
    AdminDashboardComponent,
    CategoriesComponent,
    ArticlesComponent,
    TagsComponent,
    ConfirmDialogComponent,
    BlogListComponent,
    BlogItemComponent,
    PostSamurayComponent,
    BlogCategoryComponent,
    BlogTagComponent,
    BlogNavComponent,
    BlogContactComponent,
    BlogAdminComponent,
    PostsAdminComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    DragDropModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthenticationService,
    SlugifyPipe
  ],
  bootstrap: [AppComponent],
  entryComponents: [CategoryComponent, ConfirmDialogComponent]
})
export class AppModule { }
