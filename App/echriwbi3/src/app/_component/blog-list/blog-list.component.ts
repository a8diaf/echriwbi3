import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BlogArticleService } from 'src/app/_services/blog-article.service';
import { BlogArticle } from 'src/app/_models/BlogArticle';
import { LoadingService } from 'src/app/_services/loading.service';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

  headingTopPosition: number;
  @ViewChild('blogheadingzone') blogHeadingZone: ElementRef;
  @ViewChild('blogheadingBlock') blogheadingBlock: ElementRef;


  articlesEnVedette: BlogArticle[];
  recentPosts: BlogArticle[];
  featuredPosts: BlogArticle[];
  mostReadPosts: BlogArticle[];


  constructor(private blogArticleService: BlogArticleService, private loadingService: LoadingService) {
  }

  onLoad(): void {
    this.headingTopPosition = this.blogHeadingZone.nativeElement.offsetHeight / 2 - (this.blogheadingBlock.nativeElement.offsetHeight / 2);
  }

  ngOnInit(): void {
    this.loadingService.loading = true;

    const articlesEnVedette = this.blogArticleService.getBlogArticlesEnVedette();
    const featuredposts = this.blogArticleService.getBlogFeaturedposts(3);
    const mostReadPosts = this.blogArticleService.getBlogMostReadPosts(3);
    const recentPosts = this.blogArticleService.getBlogRecentPosts(3);

    forkJoin({ articlesEnVedette, featuredposts, mostReadPosts, recentPosts })
      .subscribe(results => {

        this.articlesEnVedette = results.articlesEnVedette.content;
        this.recentPosts = results.recentPosts.content;
        this.featuredPosts = results.featuredposts.content;
        this.mostReadPosts = results.mostReadPosts.content;
        this.loadingService.loading = false;
      });


  }
}
