import { Component, OnInit } from '@angular/core';
import { BlogContactService } from 'src/app/_services/blog-contact.service';

@Component({
  selector: 'app-blog-contact',
  templateUrl: './blog-contact.component.html',
  styleUrls: ['./blog-contact.component.css']
})
export class BlogContactComponent implements OnInit {


  constructor(public blogContactService: BlogContactService) { }

  ngOnInit(): void {
  }

  submitContact() {

  }

  clearContact() {
    this.blogContactService.contactForm.reset();
    this.blogContactService.contactForm.markAsPristine();
    this.blogContactService.contactForm.markAsUntouched();
    this.blogContactService.contactForm.updateValueAndValidity();
  }

  onScriptLoad() {
    console.log('Google reCAPTCHA loaded and is ready for use!')
  }

  onScriptError() {
    console.log('Something went long when loading the Google reCAPTCHA')
  }
}
