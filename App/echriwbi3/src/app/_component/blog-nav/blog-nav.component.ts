import { Component, Input, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { BlogCategory } from 'src/app/_models/BlogCategory';
import { SlugifyPipe } from 'angular-pipes';
import { BlogCategoryService } from 'src/app/_services/blog-category.service';

@Component({
  selector: 'app-blog-nav',
  templateUrl: './blog-nav.component.html',
  styleUrls: ['./blog-nav.component.css']
})
export class BlogNavComponent implements OnInit {
  menuCategories: BlogCategory[];
  @Input()
  loading = false;
  constructor(private breakpointObserver: BreakpointObserver, private blogCategoryService: BlogCategoryService) { }
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  ngOnInit(): void {
    this.blogCategoryService.getBlogMenuCategories().subscribe((data) => {
      this.menuCategories = data.content;
    });
  }





}
