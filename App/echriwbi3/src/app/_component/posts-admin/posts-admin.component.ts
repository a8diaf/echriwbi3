import { Component, OnInit } from '@angular/core';
import { BlogArticle } from 'src/app/_models/BlogArticle';
import { BlogArticleService } from 'src/app/_services/blog-article.service';

@Component({
  selector: 'app-posts-admin',
  templateUrl: './posts-admin.component.html',
  styleUrls: ['./posts-admin.component.css']
})
export class PostsAdminComponent implements OnInit {

  constructor(private blogArticleService: BlogArticleService) { }
  posts: BlogArticle[];
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];


  ngOnInit(): void {
    this.blogArticleService.getPosts().subscribe(data => {
      this.posts = data.content;
    });
  }

}
