import { Component, OnInit, Inject } from '@angular/core';
import { CategoryService, GetResponseCategory } from 'src/app/_services/category/category.service';
import { Category } from 'src/app/_models/category';
import { NotificationService } from 'src/app/_services/notification/notification.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { catchError } from 'rxjs/operators';
import { CategorySearch } from 'src/app/_models/categorySearch';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  parentCategories: Category[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public inputData,
    public categoryService: CategoryService,
    private matDialogRef: MatDialogRef<CategoryComponent>) { }

  finalize(data) {
    if (data._links && data._links && data._links.parentCategory) {
      const link = data._links.parentCategory.href.replace('{?projection}', '');
      this.categoryService.updateCategoryParent(link).subscribe((parent) => {
        this.caregoryClear();
        this.matDialogRef.close(true);
      });
    } else {
      this.caregoryClear();
      this.matDialogRef.close(true);
    }
  }
  saveCategory() {
    if (this.categoryService.form.valid) {
      if (this.categoryService.form.get('id').value) {

        this.categoryService.updateCategory(this.categoryService.getCategoryFromForm()).subscribe((data) => {
          console.log(this.categoryService.getCategoryFromForm());
          this.finalize(data);
        });
      } else {
        this.categoryService.insertCategory(this.categoryService.getCategoryFromForm()).subscribe((data) => {
          this.finalize(data);
        });
      }
    }
  }

  caregoryClear() {
    this.categoryService.form.reset();
    this.categoryService.initForm(this.categoryService.categoryLevel);
    this.categoryService.form.markAsPristine();
    this.categoryService.form.markAsUntouched();
    this.categoryService.form.updateValueAndValidity();
  }
  parentCategoryChange() {
    this.categoryService.form.value.parentId = null;
    if ((this.categoryService.form.value.categoryLevel - 1) >= 0) {
      const categorySearch: CategorySearch = new CategorySearch();
      categorySearch.categoryLevel = this.categoryService.form.value.categoryLevel - 1;
      this.categoryService.getCategoryList(
        categorySearch,
        null,
        0,
        1000,
        null)
        .subscribe((data: GetResponseCategory) => {
          this.parentCategories = data._embedded.categories;
        });
    } else {
      this.parentCategories = [];
    }
  }
  ngOnInit(): void {
    this.parentCategoryChange();
  }
}
