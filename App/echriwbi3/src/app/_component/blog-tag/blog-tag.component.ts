import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogArticleService } from 'src/app/_services/blog-article.service';
import { LoadingService } from 'src/app/_services/loading.service';
import { BlogTagService } from 'src/app/_services/blog-tag.service';
import { BlogCategoryService } from 'src/app/_services/blog-category.service';
import { ActivatedRoute } from '@angular/router';
import { BlogCategory } from 'src/app/_models/BlogCategory';
import { BlogArticle } from 'src/app/_models/BlogArticle';
import { BlogTag } from 'src/app/_models/BlogTag';
import { forkJoin } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-blog-tag',
  templateUrl: './blog-tag.component.html',
  styleUrls: ['./blog-tag.component.css']
})
export class BlogTagComponent implements OnInit, OnDestroy {
  selectedTag: BlogTag;
  lastTagPost: BlogArticle;
  featuredPosts: BlogArticle[];
  mostReadPosts: BlogArticle[];
  tagPosts: BlogArticle[] = [];
  allCategories: BlogCategory[];
  allTags: BlogTag[];
  page: { pageSize: number; totalElements: number; totalPages: number; number: number; pageNumber: number };

  private sub: any;
  constructor(private blogArticleService: BlogArticleService,
    private loadingService: LoadingService,
    private blogTagService: BlogTagService,
    private blogCategoryService: BlogCategoryService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.loadingService.loading = true;
      const tagById = this.blogTagService.getTagById(params.id);
      const featuredposts = this.blogArticleService.getBlogFeaturedposts(3);
      const mostReadPosts = this.blogArticleService.getBlogMostReadPosts(3);
      const allCategories = this.blogCategoryService.getAllCategories();
      const allTags = this.blogTagService.getBlogAllTags();
      const pagePosts = this.blogTagService.getTagLastPostByTagId(params.id)
        .pipe(mergeMap(post => {
          const lastTagPostId = post != null ? post.id : '';
          this.lastTagPost = post;
          return this.blogTagService.getBlogTagPosts(params.id, 0, 3, lastTagPostId);
        }));

      forkJoin({ tagById, pagePosts, featuredposts, mostReadPosts, allCategories, allTags })
        .subscribe(results => {

          this.selectedTag = results.tagById;
          this.featuredPosts = results.featuredposts.content;
          this.mostReadPosts = results.mostReadPosts.content;
          this.allCategories = results.allCategories;
          this.allTags = results.allTags;
          this.tagPosts = results.pagePosts.content;
          this.page = results.pagePosts.pageable;
          this.loadingService.loading = false;
        });


    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  loadMore() {
    this.loadingService.loading = true;
    const lastCategoryPostId = this.lastTagPost != null ? this.lastTagPost.id : '';
    const pagenumber = this.page.pageNumber + 1;
    if (pagenumber < this.page.pageSize) {
      this.blogTagService.getBlogTagPosts(this.selectedTag.id, pagenumber, 3, lastCategoryPostId).subscribe((posts) => {
        Array.prototype.push.apply(this.tagPosts, posts.content);
        this.page = posts.pageable;
        this.loadingService.loading = false;
      });
    }


  }
}
