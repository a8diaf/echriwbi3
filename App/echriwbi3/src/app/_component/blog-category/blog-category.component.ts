import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogCategory } from 'src/app/_models/BlogCategory';
import { ActivatedRoute } from '@angular/router';
import { BlogCategoryService, GetResponseBlogArticle } from 'src/app/_services/blog-category.service';
import { BlogArticle } from 'src/app/_models/BlogArticle';
import { BlogTag } from 'src/app/_models/BlogTag';
import { BlogArticleService } from 'src/app/_services/blog-article.service';
import { BlogTagService } from 'src/app/_services/blog-tag.service';
import { LoadingService } from 'src/app/_services/loading.service';
import { forkJoin } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-blog-category',
  templateUrl: './blog-category.component.html',
  styleUrls: ['./blog-category.component.css']
})
export class BlogCategoryComponent implements OnInit, OnDestroy {
  selectedCategory: BlogCategory;
  lastCategoryPost: BlogArticle;

  featuredPosts: BlogArticle[];
  mostReadPosts: BlogArticle[];
  categoryPosts: BlogArticle[] = [];
  allCategories: BlogCategory[];
  allTags: BlogTag[];

  page: { pageSize: number; totalElements: number; totalPages: number; number: number; pageNumber: number };

  private sub: any;
  constructor(private blogArticleService: BlogArticleService,
    private loadingService: LoadingService,
    private blogTagService: BlogTagService,
    private blogCategoryService: BlogCategoryService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {


    this.sub = this.route.params.subscribe(params => {
      this.loadingService.loading = true;
      const categoryById = this.blogCategoryService.getCategoryById(params.id);
      const featuredposts = this.blogArticleService.getBlogFeaturedposts(3);
      const mostReadPosts = this.blogArticleService.getBlogMostReadPosts(3);
      const allCategories = this.blogCategoryService.getAllCategories();
      const allTags = this.blogTagService.getBlogAllTags();
      const pagePosts = this.blogCategoryService.getCategoryLastPostByCategoryId(params.id)
        .pipe(mergeMap(post => {
          const lastCategoryPostId = post != null ? post.id : '';
          this.lastCategoryPost = post;
          return this.blogCategoryService.getBlogCategoryPosts(params.id, 0, 3, lastCategoryPostId);
        }));


      forkJoin({ categoryById, pagePosts, featuredposts, mostReadPosts, allCategories, allTags })
        .subscribe(results => {

          this.selectedCategory = results.categoryById;
          this.featuredPosts = results.featuredposts.content;
          this.mostReadPosts = results.mostReadPosts.content;
          this.allCategories = results.allCategories;
          this.allTags = results.allTags;
          this.categoryPosts = results.pagePosts.content;
          this.page = results.pagePosts.pageable;
          this.loadingService.loading = false;
        });

    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  loadMore() {
    this.loadingService.loading = true;
    const lastCategoryPostId = this.lastCategoryPost != null ? this.lastCategoryPost.id : '';
    const pagenumber = this.page.pageNumber + 1;
    if (pagenumber < this.page.pageSize) {
      this.blogCategoryService.getBlogCategoryPosts(this.selectedCategory.id, pagenumber, 3, lastCategoryPostId).subscribe((posts) => {
        Array.prototype.push.apply(this.categoryPosts, posts.content);
        this.page = posts.pageable;
        this.loadingService.loading = false;
      });
    }


  }

}
