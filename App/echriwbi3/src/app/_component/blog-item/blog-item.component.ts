import { Component, OnInit, OnDestroy } from '@angular/core';
import { SlugifyPipe } from 'angular-pipes';
import { ActivatedRoute } from '@angular/router';
import { BlogArticle } from 'src/app/_models/BlogArticle';
import { BlogArticleService } from 'src/app/_services/blog-article.service';
import { BlogCategory } from 'src/app/_models/BlogCategory';
import { BlogCategoryService } from 'src/app/_services/blog-category.service';
import { BlogTag } from 'src/app/_models/BlogTag';
import { BlogTagService } from 'src/app/_services/blog-tag.service';
import { forkJoin } from 'rxjs';
import { LoadingService } from 'src/app/_services/loading.service';

@Component({
  selector: 'app-blog-item',
  templateUrl: './blog-item.component.html',
  styleUrls: ['./blog-item.component.css']
})
export class BlogItemComponent implements OnInit, OnDestroy {
  private sub: any;
  post: BlogArticle;
  featuredPosts: BlogArticle[];
  mostReadPosts: BlogArticle[];
  allCategories: BlogCategory[];
  allTags: BlogTag[];

  constructor(private blogCategoryService: BlogCategoryService,
    private blogArticleService: BlogArticleService,
    private blogTagService: BlogTagService,
    private route: ActivatedRoute,
    private loadingService: LoadingService,
    private slugifyPipe: SlugifyPipe) { }


  ngOnInit(): void {


    this.sub = this.route.params.subscribe(params => {
      this.loadingService.loading = true;
      const postById = this.blogArticleService.getArticleById(params.id);
      const featuredposts = this.blogArticleService.getBlogFeaturedposts(3);
      const mostReadPosts = this.blogArticleService.getBlogMostReadPosts(3);
      const allCategories = this.blogCategoryService.getAllCategories();
      const allTags = this.blogTagService.getBlogAllTags();


      forkJoin({ postById, featuredposts, mostReadPosts, allCategories, allTags })
        .subscribe(results => {

          this.post = results.postById;
          this.featuredPosts = results.featuredposts.content;
          this.mostReadPosts = results.mostReadPosts.content;
          this.allCategories = results.allCategories;
          this.allTags = results.allTags;
          this.loadingService.loading = false;
        });
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
