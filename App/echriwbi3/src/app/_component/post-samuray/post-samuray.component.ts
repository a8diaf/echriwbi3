import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SlugifyPipe } from 'angular-pipes';
import { BlogArticle } from 'src/app/_models/BlogArticle';

@Component({
  selector: 'app-post-samuray',
  templateUrl: './post-samuray.component.html',
  styleUrls: ['./post-samuray.component.css']
})
export class PostSamurayComponent implements OnInit {

  postSamurayContetLength: number = 150;

  constructor(private router: Router, private slugifyPipe: SlugifyPipe) { }

  @Input()
  layout: string;

  @Input()
  post: BlogArticle;


  ngOnInit(): void {
  }

}
