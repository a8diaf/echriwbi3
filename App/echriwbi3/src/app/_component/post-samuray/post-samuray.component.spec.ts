import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSamurayComponent } from './post-samuray.component';

describe('PostSamurayComponent', () => {
  let component: PostSamurayComponent;
  let fixture: ComponentFixture<PostSamurayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSamurayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSamurayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
