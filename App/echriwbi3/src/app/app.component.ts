import { Component, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { LoadingService } from './_services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewChecked {
  constructor(public loadingService: LoadingService, private cdRef: ChangeDetectorRef) { }
  loading = false;
  ngAfterViewChecked(): void {
    this.loading = this.loadingService.loading;
    this.cdRef.detectChanges();
  }

  onActivate(event) {
    window.scroll(0, 0);
  }
}
