import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Link } from '../_models/link';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  links: Link[] = [];
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private authenticationService: AuthenticationService,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.links.push(new Link('Admin', 'admin', '', ''));
    if (this.authenticationService.currentUserValue) {
      this.links.push(new Link('Se décconecter', '', 'logout()', 'logout'));
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['']);
  }
}
