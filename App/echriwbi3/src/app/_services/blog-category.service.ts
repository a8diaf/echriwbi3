import { Injectable } from '@angular/core';
import { BlogCategory } from '../_models/BlogCategory';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { BlogArticle } from '../_models/BlogArticle';

@Injectable({
  providedIn: 'root'
})
export class BlogCategoryService {

  constructor(private http: HttpClient) { }

  public getBlogMenuCategories(): Observable<GetResponseBlogCategory> {
    return this.http.get<GetResponseBlogCategory>(`${baseUrl}/shownonmenucategories`, {});
  }

  public getAllCategories(): Observable<BlogCategory[]> {
    return this.http.get<BlogCategory[]>(`${baseUrl}/allcategories`, {});
  }

  public getCategoryById(id: string): Observable<BlogCategory> {
    const params: HttpParams = new HttpParams().append('id', id);
    return this.http.get<BlogCategory>(`${baseUrl}/getcategorybyid`, { params });
  }

  public getCategoryLastPostByCategoryId(id: string): Observable<BlogArticle> {
    const params: HttpParams = new HttpParams().append('id', id);
    return this.http.get<BlogArticle>(`${baseUrl}/getcategorylastpostbycategoryid`, { params });
  }
  public getBlogCategoryPosts(id: string, start: number, size: number, mainpostid: string): Observable<GetResponseBlogArticle> {
    let params: HttpParams = new HttpParams().append('id', id);
    params = params.append('start', start.toString());
    params = params.append('size', size.toString());
    params = params.append('mainpostid', mainpostid);

    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/getblogcategoryposts`, { params });
  }




}
const baseUrl = `${environment.apiUrl}/blog`;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


export interface GetResponseBlogCategory {
  content: BlogCategory[];

  pageable: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
  };
}

export interface GetResponseBlogArticle {
  content: BlogArticle[];

  pageable: {
    pageSize: number,
    totalElements: number,
    totalPages: number,
    number: number,
    pageNumber: number
  };
}
