import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BlogTag } from '../_models/BlogTag';
import { BlogArticle } from '../_models/BlogArticle';

@Injectable({
  providedIn: 'root'
})
export class BlogTagService {





  constructor(private http: HttpClient) { }

  public getTagLastPostByTagId(id: string): Observable<BlogArticle> {
    const params: HttpParams = new HttpParams().append('id', id);
    return this.http.get<BlogArticle>(`${baseUrl}/gettaglastpostbytagid`, { params });
  }


  public getTagById(id: string): Observable<BlogTag> {
    const params: HttpParams = new HttpParams().append('id', id);
    return this.http.get<BlogTag>(`${baseUrl}/gettagbyid`, { params });
  }

  public getBlogAllTags(): Observable<BlogTag[]> {
    return this.http.get<BlogTag[]>(`${baseUrl}/alltags`, {});
  }

  public getBlogTagPosts(id: string, start: number, size: number, mainpostid: string): Observable<GetResponseBlogArticle> {
    let params: HttpParams = new HttpParams().append('id', id);
    params = params.append('start', start.toString());
    params = params.append('size', size.toString());
    params = params.append('mainpostid', mainpostid);

    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/getblogtagposts`, { params });
  }


}
const baseUrl = `${environment.apiUrl}/blog`;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

export interface GetResponseBlogArticle {
  content: BlogArticle[];

  pageable: {
    pageSize: number,
    totalElements: number,
    totalPages: number,
    number: number,
    pageNumber: number
  };
}
