import { TestBed } from '@angular/core/testing';

import { BlogContactService } from './blog-contact.service';

describe('BlogContactService', () => {
  let service: BlogContactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogContactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
