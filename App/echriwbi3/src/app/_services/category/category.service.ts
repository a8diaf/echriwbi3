import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Category } from 'src/app/_models/category';
import { CategorySearch } from 'src/app/_models/categorySearch';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }
  categoryLevel = 0;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.maxLength(200)),
    categoryLevel: new FormControl(this.categoryLevel, Validators.required),
    enabeled: new FormControl(true),
    parentCategory: new FormControl(null)
  });

  initForm(level: number) {
    this.categoryLevel = level;
    this.form.setValue({
      id: null,
      name: '',
      description: '',
      categoryLevel: this.categoryLevel,
      enabeled: true,
      parentCategory: null,
    });
  }
  getCategoryFromForm(): Category {
    const c: Category = new Category();
    c.categoryLevel = this.form.get('categoryLevel') ? this.form.get('categoryLevel').value : null;
    c.created = this.form.get('created') ? this.form.get('created').value : null;
    c.description = this.form.get('description') ? this.form.get('description').value : null;
    c.enabeled = this.form.get('enabeled') ? this.form.get('enabeled').value : null;
    c.id = this.form.get('id') ? this.form.get('id').value : null;
    c.name = this.form.get('name') ? this.form.get('name').value : null;
    return c;
  }

  populateForm(category: Category) {

    this.categoryLevel = category.categoryLevel;
    this.form.setValue({
      id: category.id,
      name: category.name,
      description: category.description,
      categoryLevel: category.categoryLevel,
      enabeled: category.enabeled,
      parentCategory: category.parentCategory ? category.parentCategory : null
    });
  }

  getCategoryList(
    categorySearch: CategorySearch,
    parentCategoryIds: string[],
    pageStart: number,
    pageSize: number,
    sort: string): Observable<GetResponseCategory> {

    let searchUrl = baseUrl;
    let params: HttpParams = new HttpParams().append('projection', 'exposedCategory');
    if (categorySearch) {
      if (parentCategoryIds != null && parentCategoryIds.length) {
        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < parentCategoryIds.length; index++) {
          params = params.append('categoryId', parentCategoryIds[index]);
        }
        if (categorySearch.name && categorySearch.name !== '' && !(categorySearch.categoryLevel != null)) {
          params = params.append('name', categorySearch.name);
          searchUrl = `${searchUrl}/search/findByParentCategoryIdAndNameContainingAndEnabeledTrue`;
        }
        else if (categorySearch.categoryLevel != null && !(categorySearch.name && categorySearch.name !== '')) {
          params = params.append('level', categorySearch.categoryLevel.toString());
          searchUrl = `${searchUrl}/search/findByParentCategoryIdAndCategoryLevelAndEnabeledTrue`;
        }
        else if ((categorySearch.categoryLevel != null) && (categorySearch.name && categorySearch.name !== '')) {
          params = params.append('name', categorySearch.name);
          params = params.append('level', categorySearch.categoryLevel.toString());
          searchUrl = `${searchUrl}/search/findByParentCategoryIdAndCategoryLevelAndNameContainingAndEnabeledTrue`;
        }
      }
      else if (categorySearch.name && categorySearch.name !== '' && !(categorySearch.categoryLevel != null)) {
        params = params.append('name', categorySearch.name);
        searchUrl = `${searchUrl}/search/findByNameContainingAndEnabeledTrue`;
      }
      else if (categorySearch.categoryLevel != null && !(categorySearch.name && categorySearch.name !== '')) {
        params = params.append('level', categorySearch.categoryLevel.toString());
        searchUrl = `${searchUrl}/search/findByCategoryLevelAndEnabeledTrue`;
      }
      else if ((categorySearch.categoryLevel != null) && (categorySearch.name && categorySearch.name !== '')) {
        params = params.append('name', categorySearch.name);
        params = params.append('level', categorySearch.categoryLevel.toString());
        searchUrl = `${searchUrl}/search/findByCategoryLevelAndNameContainingAndEnabeledTrue`;
      }
      else {
        searchUrl = `${searchUrl}/search/findBAndEnabeledTrue`;
      }
    } else {
      searchUrl = `${searchUrl}/search/findBAndEnabeledTrue`;
    }

    if (pageStart) {
      params = params.append('page', pageStart.toString());
    }
    if (pageSize) {
      params = params.append('size', pageSize.toString());
    }
    if (sort) {
      params = params.append('sort', sort.toString());
    }
    console.log('Request To : ' + searchUrl);
    return this.http.get<GetResponseCategory>(searchUrl, { params });
  }

  private getCategories(searchUrl: string): Observable<Category[]> {
    return this.http.get<GetResponseCategory>(searchUrl).pipe(map(response => response._embedded.categories));
  }
  public insertCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(baseUrl, category, httpOptions);
  }
  public updateCategory(category: Category): Observable<Category> {
    const editCatUrl = `${baseUrl}/${category.id}`;
    return this.http.put<Category>(editCatUrl, category, httpOptions);
  }
  public removeCategory(categoryId: string): Observable<Category> {
    const editCatUrl = `${baseUrl}/${categoryId}`;
    return this.http.patch<Category>(editCatUrl, { enabeled: false }, httpOptions);
  }

  public updateCategoryParent(categorylink: string): Observable<Category> {

    const editCatUrl = `${categorylink}`;
    if (this.form.get('parentCategory').value) {
      return this.http.put<Category>(editCatUrl,
        { _links: { parentCategory: { href: `${baseUrl}/${this.form.get('parentCategory').value}` } } }
        , httpOptions);
    } else {
      return this.http.delete<Category>(editCatUrl, httpOptions);
    }
  }

}

const baseUrl = `${environment.apiUrl}/api/categories`;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

export interface GetResponseCategory {
  _embedded: {
    categories: Category[];
  };
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
  };
}
