import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlogContactService {

  constructor(private http: HttpClient) { }

  contactForm: FormGroup = new FormGroup({
    name: new FormControl(null),
    email: new FormControl(null, Validators.email),
    message: new FormControl(null, Validators.maxLength(200)),
    recaptcha: new FormControl(false)
  });
}
