import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BlogCategory } from '../_models/BlogCategory';
import { environment } from 'src/environments/environment';
import { BlogArticle } from '../_models/BlogArticle';

@Injectable({
  providedIn: 'root'
})
export class BlogArticleService {


  constructor(private http: HttpClient) { }

  getBlogArticlesEnVedette(): Observable<GetResponseBlogArticle> {
    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/articlesenvedette`, {});
  }

  getBlogRecentPosts(num: number): Observable<GetResponseBlogArticle> {
    const params: HttpParams = new HttpParams().append('number', num.toString());
    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/recentposts`, { params });
  }

  getBlogFeaturedposts(num: number): Observable<GetResponseBlogArticle> {
    const params: HttpParams = new HttpParams().append('number', num.toString());
    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/featuredposts`, { params });
  }

  getBlogMostReadPosts(num: number): Observable<GetResponseBlogArticle> {
    const params: HttpParams = new HttpParams().append('number', num.toString());
    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/mostreadposts`, { params });
  }
  getArticleById(id: string): Observable<BlogArticle> {
    const params: HttpParams = new HttpParams().append('id', id);
    return this.http.get<BlogArticle>(`${baseUrl}/findpostbyid`, { params });
  }
  getPosts(): Observable<GetResponseBlogArticle> {
    const params: HttpParams = new HttpParams();
    return this.http.get<GetResponseBlogArticle>(`${baseUrl}/mostreadposts`, { params });
  }


}
const baseUrl = `${environment.apiUrl}/blog`;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
export interface GetResponseBlogArticle {
  content: BlogArticle[];

  pageable: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
  };
}
