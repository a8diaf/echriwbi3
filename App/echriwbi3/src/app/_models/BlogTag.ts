export class BlogTag {
  id: string;
  name: string;
  description: string;
  slug: string;

}
