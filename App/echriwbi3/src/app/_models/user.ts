export class User {
  email: number;
  username: string;
  firstName: string;
  lastName: string;
  jwt?: string;
  roles: { name: string }[];
}
