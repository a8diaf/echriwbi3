import { BlogCategory } from './BlogCategory';
import { BlogTag } from './BlogTag';

export class BlogArticle {
  id: string;
  title: string;
  slug: string;
  content: string;
  created: Date;
  category: BlogCategory;
  tags: BlogTag[];

}
