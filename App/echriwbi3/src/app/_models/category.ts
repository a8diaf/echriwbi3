export class Category {
  id: string;
  categoryLevel: number;
  created: Date;
  description: string;
  name: string;
  updated: Date;
  enabeled: boolean;
  parentCategory: string;
  // tslint:disable-next-line: variable-name
  _links: {
    parentCategory: { href: string },
    childrenCategories: { href: string },
    self: { href: string },
    category: { href: string },
    articles: { href: string }
  };
  temp: { parentCategory: { href: string; }; };

}
