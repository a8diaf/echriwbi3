export class CategorySearch {
  categoryLevel: number;
  created: Date;
  description: string;
  name: string;
  updated: Date;
}
