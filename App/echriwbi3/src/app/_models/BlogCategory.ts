export class BlogCategory {
  id: string;
  title: string;
  slug: string;
  color: string;
  articlesCount: number;
}
