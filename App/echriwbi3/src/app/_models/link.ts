export class Link {

    constructor(public name: string,
                public router: string,
                public clickFunction: string,
                public icon: string){}
}
