export class Page {
  size: number = 5;
  totalElements: number = 0;
  totalPages: number;
  number: number = 1;

  constructor(
    size: number,
    // tslint:disable-next-line: variable-name
    number: number) {
    this.number = number;
    this.size = size;
  }
}
