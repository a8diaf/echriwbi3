import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthAdminGuard } from './_helpers/auth.admin.guard';
import { HomeComponent } from './_pages/home/home.component';
import { LoginComponent } from './_pages/login/login.component';
import { AdminComponent } from './_pages/admin/admin.component';
import { AccountComponent } from './_pages/account/account.component';
import { CategoriesComponent } from './_pages/categories/categories.component';
import { TagsComponent } from './_pages/tags/tags.component';
import { ArticlesComponent } from './_pages/articles/articles.component';
import { BlogListComponent } from './_component/blog-list/blog-list.component';
import { BlogItemComponent } from './_component/blog-item/blog-item.component';
import { BlogCategoryComponent } from './_component/blog-category/blog-category.component';
import { BlogTagComponent } from './_component/blog-tag/blog-tag.component';
import { BlogContactComponent } from './_component/blog-contact/blog-contact.component';
import { BlogAdminComponent } from './_component/blog-admin/blog-admin.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'blog/category/:id/:slug', component: BlogCategoryComponent },
  { path: 'blog/tag/:id/:slug', component: BlogTagComponent },
  { path: 'blog/:id/:slug', component: BlogItemComponent },
  { path: 'blog', component: BlogListComponent },
  { path: 'blog-admin', component: BlogAdminComponent },
  { path: 'contact', component: BlogContactComponent },

  { path: 'admin-login', component: LoginComponent },
  { path: 'admin', component: AdminComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/account', component: AccountComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/categories', component: CategoriesComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/articles', component: ArticlesComponent, canActivate: [AuthAdminGuard] },
  { path: 'admin/tags', component: TagsComponent, canActivate: [AuthAdminGuard] },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
